import pyodbc
import pandas as pd
import requests
import time
server = 'MLICRMPR01'
database = 'Consumer_DW'
username = 'mohitgarg'
password = 'DaM3uKaM$123'

Access_key = "3eb3996d805f30f46443ee681185c4b8"

DATA = ["valid",
"local_format",
"international_format",
"country_prefix",
"country_code",
"country_name",
"location",
"carrier",
"line_type"]

UPDATE_SQL = '''  
    UPDATE [Consumer_DW].[dbo].[Mobile_Source]
       SET 
    valid=?,
        local_format=?,
        international_format=?,
        country_prefix=?,
        country_code=?,
        country_name=?,
        location=?,
        carrier=?,
        line_type=?,
        API_verified=?
    WHERE [EU Primary Mobile]=?
       '''


def make_connection():
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER='+ server +';DATABASE=Consumer_DW;UID=mohitgarg;PWD=DaM3uKaM$123;')
    cursor = cnxn.cursor()
    return cnxn,cursor


def update_row(data):
    row = []
    need_to_update = True
    for d in DATA:
        if d in data:
            row.append(data[d])
        else:
            row.append( '')
    if 'error' in data.keys():
        print('other error code: '+str(data['error']['code']))
        need_to_update = False
    else:
        need_to_update = True
    return row, need_to_update


def API_call(mobile):
    mobile = str(mobile)
    print(mobile)
    URL = "http://apilayer.net/api/validate?access_key="+Access_key+"&number=" + mobile+"&country_code=IN&format%20=%201"
    response = requests.get(URL)
    data1= response.json()
    print(data1)
    return update_row(data1)


def update_table():
    try:
        cnxn,cursor=make_connection()
    except:
        print("Error in connecting to the server, retrying after 30 seconds")
        time.sleep(30)
        update_table()
    cursor.execute("""SELECT DISTINCT MS.[EU Primary Mobile] FROM [Consumer_DW].[dbo].Fact_Warranty FW INNER Join [Consumer_DW].[dbo].[Dim_Product_Master] DPM ON DPM.Product_Master_ID=FW.Product_Master_ID_lkp INNER JOIN [Consumer_DW].[dbo].[Mobile_Source] MS ON (FW.[EU Primary Mobile] = MS.[EU Primary Mobile]) where CreatedOn BETWEEN '2020-01-01' AND '2020-03-31' AND DPM.LP='PL' AND MS.API_Verified is NULL;""")
    voc = cursor.fetchall()
    print(voc)
    for number_row in voc:
        number = number_row[0]
        row, need_to_update = API_call(number)
        cursor = cnxn.cursor()
        try:
            if need_to_update:
                cursor.execute(''UPDATE_SQL,row[0],row[1],row[2], row[3], row[4], row[5], row[6], row[7], row[8],1, number)
        except:
            print("Error in connecting to the server, retrying after 30 seconds")
            time.sleep(30)
            update_table()
        cnxn.commit()
        cursor.close()
    cnxn.close()



if __name__=="__main__":
    update_table()
