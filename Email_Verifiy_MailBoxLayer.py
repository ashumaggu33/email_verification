import time
import requests
import datetime
from pathlib import Path
import pyodbc
server = 'localhost'
database = 'Michelin_xverify'
username = 'Q3INFOTECH\abawa'
password = ''
table_name = 'michelin_warranty'
api_key = "0a25719440617f1218e4034d7e3141fd"
output = []
log_file = Path("email-mailbox-verify-error.log")
if log_file.exists():
    logf = open(log_file, "a+")
else:
    logf = open(log_file, "w+")
logf.write('@'*10 + '\n{} {}\n'.format("File execution started at", datetime.datetime.now(
            ).strftime('%d-%m-%Y %H:%M'))+'#'*10+'\n'*5)
columns = [
           "did_you_mean",
           "user",
           "domain",
           "format_valid",
           "mx_found",
           "smtp_check",
           "catch_all",
           "role",
           "disposable",
           "free",
           "score",
           "Account_Name",
           "MDM_Id",
           "Salutation",
           "First_Name",
           "Last_Name",
           "Phone",
           "Mobile",
           "Email",
           "Contact_ID",
           "Contact_Owner",
           "Source_Country",
           "Cluster"
           ]


def make_connection():
    db = pyodbc.connect("Driver={{FreeTDS}};PORT=1443;"
                        "Server="+server+";"
                        "Database="+database+";UID=SA;PWD=Astha@123;"
                        "Trusted_Connection=yes;")
    # import mysql.connector
    #
    # db = mysql.connector.connect(host="localhost",  # your host, usually localhost
    #                      user="root",  # your username
    #                      passwd="root",  # your password
    #                      db="mailverify", autocommit=True)
    cursor = db.cursor()
    return db, cursor


def update_row(data, email):
    row = []
    for d in columns:
        if d in data:
            row.append(data[d])
        else:
            row.append('')
    if 'error' in data.keys():
        logf.write('#'*10 + '\n{} \n{}\n'.format(email, data['error'])+'#'*10+'\n'*5)
        need_to_update = False
    else:
        need_to_update = True
    return row, need_to_update


def API_call(email):
    if str(email) == "":
        row = []
        row.append('')
        row.append('')
        row.append('')
        row.append("False")
        row.append('False')
        row.append('False')
        row.append('')
        row.append('False')
        row.append('False')
        row.append('False')
        row.append('0.0')
        return row, True
    response = requests.get("https://apilayer.net/api/check?access_key="+api_key+"&email="+str(email)+"&smtp=1&format=1")
    print("Stage 2")

    data1 = response.json()
    return update_row(data1, email)


def data_processed():
    try:
        db, cursor = make_connection()
        print("connection established")
    except Exception as e:
        print(e)
        print("Error in connecting to the server, retrying after 30 seconds")
        time.sleep(30)
        data_processed()

    cursor.execute("SELECT Email FROM mailing where API_verified is NULL limit 2, 2 ;")
    data = cursor.fetchall()
    print(data)
    print("Stage 1")
    for email_row in data:
        email = email_row[0]
        row, need_to_update = API_call(email)
        print("Stage 4")
        cursor = db.cursor()
        try:
            if need_to_update:
                val = (str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4]), str(row[5]), str(row[6]),
                               str(row[7]), str(row[8]), str(row[9]), str(row[10]), '1', email)

                cursor.execute('''
                    UPDATE mailverify.mailing
                   SET 
                    did_you_mean=%s,
                    user=%s,
                    domain=%s,
                    format_valid=%s,
                    mx_found=%s,
                    smtp_check=%s,
                    catch_all=%s,
                    role=%s,
                    disposable=%s,
                    free=%s,
                    score=%s,
                    API_verified=%s
                WHERE Email=%s
                ''', val)
                db.commit()
        except Exception as e:
            print(e)
            print("Error in connecting to the server, retrying after 30 seconds")
            print("Stage 5")
            time.sleep(30)
            data_processed()
        cursor.close()
    logf.write('@' * 10 + '\n{} {}\n'.format("File execution ends at", datetime.datetime.now(
                ).strftime('%d-%m-%Y %H:%M')) + '#' * 10 + '\n' * 5)
    db.close()


if __name__=="__main__":
    data_processed()