# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 13:13:48 2020

@author: abawa
"""

# -*- coding: utf-8 -*-
import time

"""
Created on Mon Jun  1 15:55:50 2020

@author: abawa
"""

#!/usr/bin/env python
# coding: utf-8

import requests
import datetime
import pyodbc
from pathlib import Path
server = 'localhost'
database = 'email_verify'
username = 'SA'
password = 'Astha@123'
table_name = 'email_data'
api = "Ou3wO9FLrlghjaSEUs6lB8XHN"
log_file = Path("email-millionverifier-error.log")
if log_file.exists():
    logf = open(log_file, "a+")
else:
    logf = open(log_file, "w+")
logf.write('@'*10 + '\n{} {}\n'.format("File execution started at", datetime.datetime.now(
            ).strftime('%d-%m-%Y %H:%M'))+'#'*10+'\n'*5)
output = []
columns = [
     'result',
     'resultcode',
     'subresult',
     'free',
     'role',
     'didyoumean',
     'credits',
     'executiontime',
     'error',
    "Account_Name",
       "MDM_Id",
       "Salutation",
       "First_Name",
       "Last_Name",
       "Phone",
       "Mobile",
       "Email",
       "Contact_ID",
       "Contact_Owner",
       "Source_Country",
       "Cluster"
           ]


def make_connection():
    db = pyodbc.connect("Driver={SQL Server};"
                        "Server="+server+";"
                        "Database="+database+";"
                        "UID="+username+";"
                        "PWD="+password+";")
    cursor = db.cursor()
    return db, cursor


def update_row(data, email):
    row = []
    for d in columns:
        if d in data:
            row.append(data[d])
        else:
            row.append('')
    if data['error'] != '':
        logf.write('#' * 10 + '\n{} \n{}\n'.format(email, data['error']) + '#' * 10 + '\n' * 5)
        need_to_update = False
    else:
        need_to_update = True
    return row, need_to_update


def API_call(email):
    if str(email) == "":
        row = []
        row.append('invalid')
        row.append('6')
        row.append('bad_domain')
        row.append("False")
        row.append('False')
        row.append('')
        row.append('193')
        row.append('10')
        row.append('')
        return row, True
    response = requests.get("https://api.millionverifier.com/api/v3/?api="+api+"&email="+str(email))
    data1 = response.json()
    return update_row(data1, email)


def data_processed():
    try:
        db, cursor = make_connection()
    except:
        print("Error in connecting to the server, retrying after 30 seconds")
        time.sleep(30)
        data_processed()

    cursor.execute("SELECT Email FROM ["+database+"].[dbo]."+table_name+" where API_verified is NULL;")
    data = cursor.fetchall()
    for email_row in data:
        email = email_row[0]
        row, need_to_update = API_call(email)
        cursor = db.cursor()
        try:
            if need_to_update:
                val = (str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4]), str(row[5]), str(row[6]),
                       str(row[7]), str(row[8]), '1', email)

                cursor.execute('''
                                    UPDATE michelin_warranty
                                   SET 
                                    result=?,
                                    resultcode=?,
                                    subresult=?,
                                    free=?,
                                    role=?,
                                    didyoumean=?,
                                    credits=?,
                                    executiontime=?,
                                    error=?,
                                    API_verified=?
                                WHERE Email=?
                                ''', val)
        except Exception as e:
            print(e)
            print("Error in connecting to the server, retrying after 30 seconds")
            time.sleep(30)
            data_processed()
        db.commit()
        cursor.close()
    db.close()


if __name__ == "__main__":
    data_processed()
