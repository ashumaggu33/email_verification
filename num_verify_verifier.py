import time
import requests
import datetime
from pathlib import Path
import pyodbc
server = 'ABAWA-LAPTOP\SQLEXPRESS'
database = 'Michelin_xverify'
username = 'Q3INFOTECH\abawa'
password = ''
table_name = 'michelin_warranty'
api_key = "9a96ae15ce122830c7f881c070631f71"
output = []
log_file = Path("number-verifier-error.log")
if log_file.exists():
    logf = open(log_file, "a+")
else:
    logf = open(log_file, "w+")
logf.write('@'*10 + '\n{} {}\n'.format("File execution started at", datetime.datetime.now(
            ).strftime('%d-%m-%Y %H:%M'))+'#'*10+'\n'*5)
columns = [
           "valid",
           "number",
           "local_format",
           "international_format",
           "country_prefix",
           "country_code",
           "country_name",
           "location",
           "carrier",
           "line_type",
           "Account_Name",
           "MDM_Id",
           "Salutation",
           "First_Name",
           "Last_Name",
           "Phone",
           "Mobile",
           "Email",
           "Contact_ID",
           "Contact_Owner",
           "Source_Country",
           "Cluster"
           ]


def make_connection():
    db = pyodbc.connect("Driver={SQL Server};"
                        "Server="+server+";"
                        "Database="+database+";"
                        "Trusted_Connection=yes;")
    # import mysql.connector
    #
    # db = mysql.connector.connect(host="localhost",  # your host, usually localhost
    #                      user="root",  # your username
    #                      passwd="root",  # your password
    #                      db="mailverify", autocommit=True)
    cursor = db.cursor()
    return db, cursor


def update_row(data, number):
    row = []
    for d in columns:
        if d in data:
            row.append(data[d])
        else:
            row.append('')
    if 'error' in data.keys():
        logf.write('#'*10 + '\n{} \n{}\n'.format(number, data['error'])+'#'*10+'\n'*5)
        need_to_update = False
    else:
        need_to_update = True
    return row, need_to_update


def API_call(number, country_code1):
    response = requests.get("https://apilayer.net/api/validate?access_key="+api_key+"&number="+str(number)+"&country_code="+str(country_code1)+"&format=1")
    print("Stage 2")

    data1 = response.json()
    return update_row(data1, number)


def data_processed():
    try:
        db, cursor = make_connection()
        print("connection established")
    except Exception as e:
        print(e)
        print("Error in connecting to the server, retrying after 30 seconds")
        time.sleep(30)
        data_processed()

    cursor.execute("SELECT Phone, Source_Country FROM [Michelin_xverify].[dbo].[michelin_warranty] where API_verified is NULL;")
    data = cursor.fetchall()
    print(data)
    print("Stage 1")
    for mobile_row in data:
        mobile = mobile_row[0]
        country_code1 = mobile_row[1]
        row, need_to_update = API_call(mobile, country_code1)
        print("Stage 4")
        cursor = db.cursor()
        try:
            if need_to_update:
                val = (str(row[0]), str(row[1]), str(row[2]), str(row[3]), str(row[4]), str(row[5]), str(row[6]),
                       str(row[7]), str(row[8]), str(row[9]), '1', mobile)

                cursor.execute('''
                    UPDATE michelin_warranty
                   SET 
                    valid=?,
                    number=?,
                    local_format=?,
                    international_format=?,
                    country_prefix=?,
                    country_code=?,
                    country_name=?,
                    location=?,
                    carrier=?,
                    line_type=?,
                    API_verified=?
                WHERE Phone=?
                ''', val)
                db.commit()
        except Exception as e:
            print(e)
            print("Error in connecting to the server, retrying after 30 seconds")
            print("Stage 5")
            time.sleep(30)
            data_processed()
        cursor.close()
    logf.write('@' * 10 + '\n{} {}\n'.format("File execution ends at", datetime.datetime.now(
                ).strftime('%d-%m-%Y %H:%M')) + '#'*10 + '\n'*5)
    db.close()


if __name__ == "__main__":
    data_processed()
